package sda.model;

public class OrderedProduct {
    private int id;
    private Product product;
    private Meterial material;
    private int quantiity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Meterial getMaterial() {
        return material;
    }

    public void setMaterial(Meterial material) {
        this.material = material;
    }

    public int getQuantiity() {
        return quantiity;
    }

    public void setQuantiity(int quantiity) {
        this.quantiity = quantiity;
    }
}

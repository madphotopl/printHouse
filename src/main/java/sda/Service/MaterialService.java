package sda.Service;

import sda.Service.repository.MaterialRepository;
import sda.Service.repository.ProductRepository;
import sda.model.Meterial;
import sda.model.Product;

import java.util.List;

public class MaterialService {
    private MaterialRepository repository;

    public MaterialService(MaterialRepository repository) {
        this.repository = repository;
    }

    public List<Meterial> getAll() {
        return repository.getAll();
    }

    public Meterial getById(int id) {
        return repository.getById(id);
    }
}

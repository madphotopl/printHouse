package sda.Service;

import sda.Service.repository.ProductRepository;
import sda.model.Meterial;
import sda.model.Product;

import java.util.List;

public class ProductService {
    private ProductRepository repository;
    public ProductService (ProductRepository repository){
        this.repository = repository;
    }
    public List<Product>getAll(){
        return repository.getAll();
    }
    public Product getById(int id){
        return repository.getById(id);
    }
    public List<Meterial>getMaterialsByProduct(Product product){
        return repository.getMaterialsByProduct(product.getId());
    }

}

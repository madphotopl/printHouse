package sda.Service;

import sda.Service.repository.UserRepo;
import sda.model.User;

public class UserService {
    private UserRepo repository;

  public UserService(UserRepo repository){
      this.repository = repository;
  }

    public void register(User user) {
      repository.save(user);
      }
      public void update(User user) {
      repository.update(user);
      }
      public User getById(int id) {
       return repository.getById(id);
      }

}

package sda.Service;

import sda.Service.repository.OrderRepository;
import sda.model.Order;

import java.util.List;

public class OrderService {
    private OrderRepository repository;

    public OrderService(OrderRepository repository) {
        this.repository = repository;
    }

    public void save(Order order) {
        throw new UnsupportedOperationException();
    }

    public List<Order> getByUserId(int id) {
        return repository.getByUserId(id);
    }

    public List<Order> getById(int id) {
        return repository.getById(id);
    }

    public List<Order> getAll() {
        return repository.getAll();
    }
}
